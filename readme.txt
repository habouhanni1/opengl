-        Installation de l'application      -
---------------------------------------------
	Lors de ce projet, nous avons reproduit un environnement similaire � celui recommand� par M. Malgouyres, � savoir une distribution linux (Ubuntu) avec une carte graphique supportant OpenGL 3.0. Nous avons utilis� les biblioth�ques SDL2, OpenGL, GLU3 et GLU. En outre, nous avons import� les librairies ASSIMP3.

	Pour la compilation, on a utilis� l'outil CMake. Le fichier CMakeLists.txt inclut nos diff�rents fichiers .cpp et plusieurs options garantissant une meilleure qualit� et stabilit� du code.


-              Diagramme UML               -
--------------------------------------------
	Notre conception de la solution est rest�e en grande partie fid�le � celle propos�e sur le cours. Une conception qui s'adapte au pattern MVC.
Le mod�le contient les donn�es d�finissant l'�tat de notre application : cam�ra, les param�tres de la projection 3D->2D ; donn�es concernant la lumi�re; donn�es concernant les objets � afficher et leurs propri�t�s mat�rielles (couleur, texture, etc.). ainsi qu'une m�thode Update de mise � jour.
	La couche Vue mat�rialis�e par la classe DisplayManager qui permet de g�rer les dimensions et l'affichage de la vue. 
Une classe EventContrller faisant office de contr�leur et permet de faire l'inventaire des �v�nements clavier et souris auxquels l'application doit r�pondre en appelant les fonctions de callback correspondantes ainsi que la texture et l'eclairage.
	Des classes utilitaires qui r�pondent � des besoins sp�cifiques (framesData, MouseData).
La classe WrapperSDL permet de rendre le code ind�pendant de l'API SDL, pour une meilleure �volutivit�.
Et finalement une classe MainApplication constituant le point d'entr�e de notre application.




- 	        R�alisation                -
--------------------------------------------
	En r�pondant aux questions des diff�rents TPs, nous avons pu r�pondre aux attentes minimales du projets. En effet, notre solution, contenant une sc�ne o� il est possible de d�placer la cam�ra selon diff�rents axes, propose l'objet � afficher (objs). A cet objet est appliqu� une texture. Cet objet diffuse la lumi�re selon le type de la source lumineuse.  

	N�anmoins, nous n'avons pas pu, en raison du manque du temps et la complexit� de certains concepts li�s � la programmation par shaders, r�pondre aux attentes suppl�mentaires. Nous avons eu des probl�mes aussi au niveau de l'application des textures.


