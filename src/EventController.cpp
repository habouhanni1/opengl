

#include <cstdlib>
#include <cstdio>

#include "MouseData.h"
#include "WrapperSDL.h"
#include "Modele.h"

// Initialisation des données de classe :
SDL_TimerID WrapperSDL::EventController::mTimerId;
double MouseData::teta=0;

void WrapperSDL::EventController::Init(SDL_Window *p_window, Modele *p_modele) {
    // Enregistrement d'un timer de raffraichissement de la vue
    mTimerId = SDL_AddTimer(20, CreateTimerRefreshFrame, (void *) p_modele);
    EventController::DoEventsLoop(p_window, p_modele);

}

void WrapperSDL::EventController::DoEventsLoop(SDL_Window *p_window, Modele *p_modele) {
    bool terminer = false;
    SDL_Event evenement; // union contenant un évennement

    while (!terminer) {
        while (SDL_PollEvent(&evenement)) {  // on défile les évennements
            // Gestion de l'événement et modification des variables
            terminer = Handle_SDL_Event(&evenement, p_window, p_modele);
        }
    }
}

Uint32 WrapperSDL::EventController::CreateTimerRefreshFrame(Uint32 interval, void *p_modele) {

    SDL_UserEvent userevent;
    userevent.type = SDL_USEREVENT;

//    userevent.code = UserEventKinds::ANIMATION_TIMER;
    // Initialisation des données de l'événement nécessaire à l'affichage
    userevent.data1 = (void *) p_modele; // fonction de raffraichissement de la vue
    userevent.data2 ; // On ne l'utilise pas ici

    // On crée l'événement pour l'invocation suivante
    SDL_Event event;
    event.type = SDL_USEREVENT;
    event.user = userevent;
    SDL_PushEvent(&event); // On ajoute l'événement dans la file

    return interval;
}

/**
 * @brief Gestion d'un événement SDL extrait de la file
 * @param p_evenement données de l'événement
 * @param window  Fenêtre SDL (pour gérer les SDL_WINDOWEVENT)
 * @param p_ParamsAffichage instance de la classe Vue
 * @return true si l'événement est SDL_QUIT (fermeture de la fenêtre)
 */
bool WrapperSDL::EventController::Handle_SDL_Event(const SDL_Event *p_evenement,
                                                   SDL_Window *p_window, Modele *p_modele) {
    double oldPostionValue = 0;
    double changement=-100;
    switch (p_evenement->type) { // suivant le type d'événement
        //////////////////////////////////////////////////////
        case SDL_KEYDOWN:
            printf("La touche %s a été enfoncée\n",
                   SDL_GetKeyName(p_evenement->key.keysym.sym));
            if(p_evenement->key.keysym.mod & KMOD_SHIFT) {
                changement = -changement;
            }
            switch (p_evenement->key.keysym.sym) {
                /*case SDLK_s:
                    p_modele->SetSelection(2);
                    p_modele->Update();
                    SDL_GL_SwapWindow(p_window);
                    break;*/
                case SDLK_t:
                    p_modele->SetSelection(1);
                    p_modele->Update();
                    SDL_GL_SwapWindow(p_window);
                    break;
                case SDLK_p:
                    p_modele->SetSelection(0);
                    p_modele->Update();
                    SDL_GL_SwapWindow(p_window);
                    break;
                case SDLK_x:
                    oldPostionValue = p_modele->getCamera().getMPosition()[0];
                    const_cast<Camera *>(&(p_modele->getCamera()))->SetpositionX(oldPostionValue + changement);
                    p_modele->Update();
                    SDL_GL_SwapWindow(p_window);
                    break;
                case SDLK_y :
                    oldPostionValue= p_modele->getCamera().getMPosition()[1];
                    const_cast<Camera *>(&(p_modele->getCamera()))->SetpositionY(oldPostionValue + changement);
                    p_modele->Update();
                    SDL_GL_SwapWindow(p_window);
                    break;
                case SDLK_z :
                    oldPostionValue = p_modele->getCamera().getMPosition()[2];
                    const_cast<Camera *>(&(p_modele->getCamera()))->SetpositionZ(oldPostionValue + changement);
                    p_modele->Update();
                    SDL_GL_SwapWindow(p_window);
                    break;
                case SDLK_UP:
                    oldPostionValue= p_modele->getCamera().getMPosition()[1];
                    const_cast<Camera *>(&(p_modele->getCamera()))->SetpositionY(oldPostionValue + 100);
                    p_modele->Update();
                    SDL_GL_SwapWindow(p_window);
                    break;
                case SDLK_DOWN:
                    oldPostionValue= p_modele->getCamera().getMPosition()[1];
                    const_cast<Camera *>(&(p_modele->getCamera()))->SetpositionY(oldPostionValue - 100);
                    p_modele->Update();
                    SDL_GL_SwapWindow(p_window);
                    break;
                case SDLK_LEFT:
                    oldPostionValue= p_modele->getCamera().getMPointDeVisee()[1];
                    const_cast<Camera *>(&(p_modele->getCamera()))->SetpointDeViseY(oldPostionValue - 5);
                    p_modele->Update();
                    SDL_GL_SwapWindow(p_window);
                    break;
                case SDLK_RIGHT:
                    oldPostionValue= p_modele->getCamera().getMPointDeVisee()[1];
                    const_cast<Camera *>(&(p_modele->getCamera()))->SetpointDeViseY(oldPostionValue + 5);
                    p_modele->Update();
                    SDL_GL_SwapWindow(p_window);
                case SDLK_e:
                    oldPostionValue = p_modele->getCamera().getMPosition()[0];
                    const_cast<Camera *>(&(p_modele->getCamera()))->SetpositionX(1.1* (oldPostionValue) + p_modele->getCamera().getMPointDeVisee()[0]);
                    oldPostionValue = p_modele->getCamera().getMPosition()[1];
                    const_cast<Camera *>(&(p_modele->getCamera()))->SetpositionY(1.1* (oldPostionValue) + p_modele->getCamera().getMPointDeVisee()[1]);
                    oldPostionValue = p_modele->getCamera().getMPosition()[2];
                    const_cast<Camera *>(&(p_modele->getCamera()))->SetpositionZ(1.1* (oldPostionValue) + p_modele->getCamera().getMPointDeVisee()[2]);
                    p_modele->Update();
                    SDL_GL_SwapWindow(p_window);
                    break;
                case SDLK_r :
                    oldPostionValue = p_modele->getCamera().getMPosition()[0];
                    const_cast<Camera *>(&(p_modele->getCamera()))->SetpositionX(((oldPostionValue) - p_modele->getCamera().getMPointDeVisee()[0])/1.1);
                    oldPostionValue = p_modele->getCamera().getMPosition()[1];
                    const_cast<Camera *>(&(p_modele->getCamera()))->SetpositionY(((oldPostionValue) - p_modele->getCamera().getMPointDeVisee()[1])/1.1);
                    oldPostionValue = p_modele->getCamera().getMPosition()[2];
                    const_cast<Camera *>(&(p_modele->getCamera()))->SetpositionZ(((oldPostionValue) - p_modele->getCamera().getMPointDeVisee()[2])/1.1);
                    p_modele->Update();
                    SDL_GL_SwapWindow(p_window);
                    break;
                case SDLK_a:
                    oldPostionValue = p_modele->getCamera().getMAngleOuvertureY();
                    const_cast<Camera *>(&(p_modele->getCamera()))->SetMAngleOuvertureY(oldPostionValue + changement* M_PI/1800);
                    p_modele->getCamera().ApplyPerspectiveProjection();
                    p_modele->Update();
                    SDL_GL_SwapWindow(p_window);
                    break;
                default:
                    break;
            }
            break;

        // Événements utilisateur via la souris
        case SDL_MOUSEBUTTONDOWN: // Enfoncement d'un bouton souris
            switch (p_evenement->button.button) {
                case SDL_BUTTON_RIGHT :  // Bouton gauche
                    MouseData::rightButtonPressed = true;
                    MouseData::mousex = p_evenement->button.x; // mémorisation coordonnées souris
                    MouseData::mousey = p_evenement->button.y; // mémorisation coordonnées souris
                    break;
                case SDL_BUTTON_LEFT :  // Bouton gauche
                    MouseData::leftButtonPressed = true;
                    MouseData::mousex = p_evenement->button.x; // mémorisation coordonnées souris
                    MouseData::mousey = p_evenement->button.y; // mémorisation coordonnées souris
                    break;
                case SDL_BUTTON_MIDDLE :  // Bouton gauche
                    MouseData::middleButtonPressed = true;
                    MouseData::mousex = p_evenement->button.x; // mémorisation coordonnées souris
                    MouseData::mousey = p_evenement->button.y; // mémorisation coordonnées souris
                    break;
                default:;
            }
            break;
        case SDL_MOUSEBUTTONUP: // Relachement d'un bouton souris
            switch (p_evenement->button.button) {
                case SDL_BUTTON_RIGHT :  // Bouton gauche
                    MouseData::rightButtonPressed = false;
                    break;
                case SDL_BUTTON_LEFT :  // Bouton gauche
                    MouseData::leftButtonPressed = false;
                    break;
                case SDL_BUTTON_MIDDLE :  // Bouton gauche
                    MouseData::middleButtonPressed = false;
                    break;
                default:;
            }
            break;
        case SDL_MOUSEMOTION: // Mouvement de la souris
            if (MouseData::rightButtonPressed) {
                // Mise à jour du modèle
                p_modele->UpdateMouseMotion(p_evenement->motion.x - MouseData::mousex,
                                            p_evenement->motion.y - MouseData::mousey);
                MouseData::mousex = p_evenement->motion.x; // enregistrement des nouvelles
                MouseData::mousey = p_evenement->motion.y; // coordonnées de la souris
                SDL_GL_SwapWindow(p_window);
            }
            if(MouseData::leftButtonPressed){
                MouseData::teta +=((p_evenement->motion.x-MouseData::mousex)*M_PI/(1800*3));
                p_modele->rotateView(MouseData::teta);
                SDL_GL_SwapWindow(p_window);
            }
            break;

            //////////////////////////////////////////////////////
            // Événements perso : raffraîchissement de la vue
        case SDL_USEREVENT: // Événement timer
            // On teste la sorte d'événement SDL_USEREVENT (on peut en définir plusieurs)
         /*   if (p_evenement->user.code == UserEventKinds::ANIMATION_TIMER) {
                // Récupération des données de l'événement nécessaire à l'affichage
                // Ici, commenté car redondant avec le paramètre p_modele de la méthode
                //Modele *p_modele = static_cast<Modele *>(p_evenement->user.data1);

                // Mise à jour du modèle et de la vue :
                p_modele->Update();

                // On envoie le buffer à l'écran à 50 FPS suivant le timer
                SDL_GL_SwapWindow(p_window);
            }
            break;*/
            //////////////////////////////////////////////////////
            // Événements utilisateur sur la fenêtre graphique
        case SDL_WINDOWEVENT: // La fenêtre graphique a changé
            int w, h;
            SDL_GetWindowSize(p_window, &w, &h); // récupération taille fenêtre
            // Application des changements sur le modèle de projection 3D->2D
            p_modele->Redimensionnement(w, h);
            break;
            //////////////////////////////////////////////////////
            // Fermeture de l'application
        case SDL_QUIT: // fermeture de la fenêtre
            return true;
            break;
        default:
            fprintf(stderr, "Événement non géré\n");
    }
    return false;
}
