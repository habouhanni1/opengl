
#ifndef HEADER_MATERIAL_H
#define HEADER_MATERIAL_H

#include <cstdlib>

class Material{
	float mAmbient[4];
	float mDiffuse[4];
	float mSpecular[4];
	float  mShininess;
	
	public:
		Material(float ambientR,float ambientG,float ambientB,
				float diffuseR,float diffuseG,float diffuseB,
				float specularR,float specularG,float specularB,
				float  shininess);
				
		const float *getAmbient()const;
		const float *getDiffuse()const;
		const float *getSpecular()const;
				
		const float getShininess()const;
};
#endif 
