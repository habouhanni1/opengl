

#include <GLES3/gl3.h>
#include <GL/glut.h>
#include <iostream>
#include "RenderingGlobalRoutines.h"
#include "Material.h"
#include  "Modele.h" 

void RenderingGlobalRoutines::Init() {
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glColor3f(0.0, 0.0, 0.0);
    glEnable(GL_DEPTH_TEST);
}

void RenderingGlobalRoutines::InitView() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void RenderingGlobalRoutines::drawTeaPot(GLdouble size) {
    glutWireTeapot(size);
}

//scene exists in assimp wrapper
void RenderingGlobalRoutines::drawScene() {
    aiMesh *mesh;
    glBegin(GL_POINTS);
    for (unsigned int m = 0; m < WrapperAssimp::getInstance()->getScene()->mNumMeshes; m++) {
        mesh = (WrapperAssimp::getInstance()->getScene()->mMeshes)[m];
        for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
            for (unsigned int j = 0; j < mesh->mFaces[i].mNumIndices; j++) {
                //aiVector3D pos = mesh->mVertices[aiface.mIndices[j]];
                int k = mesh->mFaces[i].mIndices[j];
                glVertex3f(mesh->mVertices[k].x, mesh->mVertices[k].y, mesh->mVertices[k].z);
            }
        }
    }
    glEnd();
}

void RenderingGlobalRoutines::drawSolarSystem(double angleRotation) {
    double rayon = 5;
    glPushMatrix();
    glRotated(angleRotation / 4, 0, 0, -1);
    glTranslated(6 * rayon, 0, 0);
    glRotated(angleRotation, 0, 0, -1);
    glutWireSphere(rayon, 20, 10);
    glPopMatrix();
    glutWireSphere(2 * rayon, 20, 10);

}



void RenderingGlobalRoutines::DrawModel(const Modele &modele) {
    switch(modele.getSelection())
    {
        case 0:
            drawSolarSystem(modele.getMAngleRotation());
            break;
        case 1:
            drawTeaPot(5);
            break;
        case 2:
            drawScene();
            break;
        default:
            break;
    }
}



void RenderingGlobalRoutines::ApplyPointLightPosition(int32_t lightId, const float position[4]){
	glLightfv(lightId,GL_POSITION,position);

}
void RenderingGlobalRoutines::ApplyPointLightIntensity(int32_t lightId, const float diffuseIntensity[4],const float specularIntensity[4])
{
	glLightfv(lightId,GL_DIFFUSE,diffuseIntensity);
	glLightfv(lightId,GL_SPECULAR,specularIntensity);
	glEnable(lightId);
}
 void RenderingGlobalRoutines::DisablePointLight(int32_t lightId)
{
		glDisable(lightId);
}
void RenderingGlobalRoutines::ApplyMaterial(Material material){
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,material.getAmbient());
	glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,material.getDiffuse());
	glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,material.getSpecular());
	glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,material.getShininess());
}

