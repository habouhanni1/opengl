/******************************************************************************\
*     Copyright (C) 2017 by Rémy Malgouyres                                    * 
*     http://malgouyres.org                                                    * 
*     File: DisplayManager.cpp                                                 * 
*                                                                              * 
* The program is distributed under the terms of the GNU General Public License * 
*                                                                              * 
\******************************************************************************/


#include "DisplayManager.h"
#include "Modele.h"
#include "FramesData.h"
#include "RenderingGlobalRoutines.h"
#include "Camera.h"
#include "GeometricTransform.h"
#include <stdio.h>

DisplayManager::DisplayManager() :
        mWindowChanged(true) {
    RenderingGlobalRoutines::Init();
}

void DisplayManager::setWindowChanged() {
    mWindowChanged = true;
}

void DisplayManager::Affichage(const Modele &modele) {
    // Si les dimensions de la fenêtre ont changé (ou à l'initialisation)
    if (mWindowChanged) {
        Redimensionnement(modele); // (re-)Initialisation de la projection 3D --> 2D
        mWindowChanged = false;
    }
    RenderingGlobalRoutines::InitView();
    Camera::ClearModelView();
    modele.getLightSources().ApplyLightPositions(1);
    modele.getCamera().ApplyCameraCoordinates();
    modele.getLightSources().ApplyLightPositions(0);
    modele.getLightSources().ApplyLightIntensities();
    modele.ApplyModelTransform();
    RenderingGlobalRoutines::ApplyMaterial(modele.getDefaultMaterial());
    RenderingGlobalRoutines::DrawModel(modele);

}

void DisplayManager::Redimensionnement(const Modele &modele) {
    // Surface de rendu (voir chapitres suivants)

    GeometricTransform::Viewport(0, 0, (GLsizei) modele.getLargeurFenetre(),
                                 (GLsizei) modele.getHauteurFenetre());
    Camera::ClearProjection();
    modele.getCamera().ApplyPerspectiveProjection();
}
