//
// Created by mohamed on 10/11/17.
//

#include <iostream>
#include "WrapperAssimp.h"

WrapperAssimp *WrapperAssimp::sInstance ;
Assimp::Importer WrapperAssimp::importer;

WrapperAssimp::WrapperAssimp() : mPath(){
}

WrapperAssimp *WrapperAssimp::getInstance() {
    if (!sInstance) {
        sInstance = new WrapperAssimp();
    }
    return sInstance;
}

const aiScene *WrapperAssimp::getScene() const {
    return mScene;
}

void WrapperAssimp::rechargeScene(std::string s) {
    mScene = importer.ReadFile(s, aiProcess_FindDegenerates|
                                  aiProcess_SortByPType |
                                  aiProcess_Triangulate);

    // If the import failed, report it
    if (!mScene) {
        std::cout << "erreur : " << importer.GetErrorString() << std::endl;
        return;
    }
    std::cout << "Scene uploaded successfully" << std::endl;
}

