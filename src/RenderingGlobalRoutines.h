
#ifndef PROJECT_RENDERINGGLOBALROUTINES_H
#define PROJECT_RENDERINGGLOBALROUTINES_H
#include <GLES3/gl3.h>
#include <GL/glut.h>

#include "WrapperAssimp.h"


class Modele;
class Material;

struct RenderingGlobalRoutines {
    static void Init();
    static void InitView();
    static void DrawModel(const Modele& modele);
    static void ApplyPointLightPosition(int32_t lightId, const float position[4]);
    static void ApplyPointLightIntensity(int32_t lightId, const float diffuseIntensity[4],const float SpecularIntensity[4]);
    static void DisablePointLight(int32_t lightId);
    static void ApplyMaterial(Material material);

    static void drawTeaPot(GLdouble size);
static void drawScene();
static void drawSolarSystem(double);
};


#endif //PROJECT_RENDERINGGLOBALROUTINES_H
