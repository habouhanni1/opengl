

#ifndef PROJECT_WRAPPERASSIMP_H
#define PROJECT_WRAPPERASSIMP_H


#include <string>
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

class WrapperAssimp {
private :
    std::string mPath;
    const aiScene* mScene;
    static Assimp::Importer importer;
    static WrapperAssimp *sInstance;
    WrapperAssimp();
    WrapperAssimp(const WrapperAssimp&) = delete;
public:
    void operator=(const WrapperAssimp&)= delete;
    static WrapperAssimp* getInstance();
    const aiScene *getScene() const;
    void rechargeScene(std::string);
};


#endif //PROJECT_WRAPPERASSIMP_H
