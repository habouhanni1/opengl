
#include <cstring>
#include "Camera.h"
#include "GeometricTransform.h"

Camera::Camera(double angleOuvertureY, double aspect, double z_proche, double z_eloigne, double positionX,
               double positionY, double positionZ, double pointDeViseeX, double pointDeViseeY, double pointDeViseeZ,
               double vecteurVerticalX, double vecteurVerticalY, double vecteurVerticalZ) :
        mAngleOuvertureY(angleOuvertureY),
        mAspect(aspect),
        mZ_proche(z_proche),
        mZ_eloigne(z_eloigne) {
    mPosition[0] = positionX;
    mPosition[1] = positionY;
    mPosition[2] = positionZ;
    mPointDeVisee[0] = pointDeViseeX;
    mPointDeVisee[1] = pointDeViseeY;
    mPointDeVisee[2] = pointDeViseeZ;
    mVecteurVertical[0] = vecteurVerticalX;
    mVecteurVertical[1] = vecteurVerticalY;
    mVecteurVertical[2] = vecteurVerticalZ;

}

void Camera::SetProjection(double angleOuvertureY, double aspect, double z_proche, double z_eloigne) {
    mAngleOuvertureY = angleOuvertureY;
    mAspect = aspect;
    mZ_proche = z_proche;
    mZ_eloigne = z_eloigne;
}

void Camera::LookAt(double *position, double *pointDeVisee, double *vecteurVertical) {
    memcpy(mPosition, position, 3 * sizeof(double));
    memcpy(mPointDeVisee, pointDeVisee, 3 * sizeof(double));
    memcpy(mVecteurVertical, vecteurVertical, 3 * sizeof(double));
}

void Camera::UpdateAspect(double aspect) {
    SetProjection(mAngleOuvertureY, aspect, mZ_proche, mZ_eloigne);
}

void Camera::ApplyPerspectiveProjection() const {
    GeometricTransform::ApplyPerspectiveProjection(mAngleOuvertureY,mAspect,mZ_proche,mZ_eloigne);
}

void Camera::ClearModelView() {
    GeometricTransform::ClearModelView();
}

void Camera::ClearProjection() {
    GeometricTransform::ClearProjection();
}

void Camera::ApplyCameraCoordinates() const {
    GeometricTransform::LookAt(mPosition,mPointDeVisee,mVecteurVertical);
}

void Camera::SetpositionX(double x_valeur) {
    mPosition[0]=x_valeur;
}

void Camera::SetpositionY(double y_valeur) {
    mPosition[1]=y_valeur;
}

void Camera::SetpositionZ(double z_valeur) {
    mPosition[2]=z_valeur;
}

const double *Camera::getMPosition() const {
    return mPosition;
}

void Camera::SetpointDeViseX(double x_valeur) {
    mPointDeVisee[0]=x_valeur;
}

void Camera::SetpointDeViseY(double y_valeur) {
    mPointDeVisee[1]=y_valeur;
}

void Camera::SetpointDeViseZ(double z_valeur) {
    mPointDeVisee[2]=z_valeur;
}

const double *Camera::getMPointDeVisee() const {
    return mPointDeVisee;
}

double Camera::getMAngleOuvertureY() const {
    return mAngleOuvertureY;
}

void Camera::SetMAngleOuvertureY(double AngleOuvertureY) {
    Camera::mAngleOuvertureY = AngleOuvertureY;
}

void Camera::SetMAspect(double Aspect) {
    Camera::mAspect = Aspect;
}

const double *Camera::getMVecteurVertical() const {
    return mVecteurVertical;
}
