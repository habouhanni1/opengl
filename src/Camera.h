
#ifndef SOURCES_OPENGL_SDL_CAMERA_H
#define SOURCES_OPENGL_SDL_CAMERA_H

/** Types de repères supportés (repères du monde et de la caméra) */
  /*enum class TypeRepere{
    MONDE = 0, CAMERA = 1
  };*/

class Camera {
    double mAngleOuvertureY;
    double mAspect;
    double mZ_proche;
    double mZ_eloigne;

    double mPosition[3];
    double mPointDeVisee[3];
    double mVecteurVertical[3];

public:
    Camera(double angleOuvertureY, double aspect,
           double z_proche, double z_eloigne,
           double positionX, double positionY, double positionZ,
           double pointDeViseeX, double pointDeViseeY, double pointDeViseeZ,
           double vecteurVerticalX, double vecteurVerticalY, double vecteurVerticalZ);
	
    void SetpositionX(double x_valeur);

    void SetpositionY(double y_valeur);

    void SetpositionZ(double z_valeur);

    void SetpointDeViseX(double x_valeur);

    void SetpointDeViseY(double y_valeur);

    void SetpointDeViseZ(double z_valeur);

    void SetMAspect(double mAspect);

    const double *getMVecteurVertical() const;

    double getMAngleOuvertureY() const;

    void SetMAngleOuvertureY(double mAngleOuvertureY);

    const double *getMPosition() const;

    const double *getMPointDeVisee() const;

    void SetProjection(double angleOuvertureY, double aspect, double z_proche, double z_eloigne);

    void LookAt(double position[3], double pointDeVisee[3], double vecteurVertical[3]);

    void UpdateAspect(double aspect);

    void ApplyPerspectiveProjection() const;

    static void ClearModelView();

    static void ClearProjection();

    void ApplyCameraCoordinates() const;

};


#endif //SOURCES_OPENGL_SDL_CAMERA_H
