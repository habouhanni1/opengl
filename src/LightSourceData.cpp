/******************************************************************************\
*     Copyright (C) 2017 by Rémy Malgouyres                                    * 
*     http://malgouyres.org                                                    * 
*     File: LightSourceData.cpp                                                * 
*                                                                              * 
* The program is distributed under the terms of the GNU General Public License * 
*                                                                              * 
\******************************************************************************/ 

#include "LightSourceData.h"

LightSourceData::LightSourceData()
        :mSourcesRepereCamera(),
         mSourcesRepereMonde()
{}


  /** Types de repères supportés (repères du monde et de la caméra) */
  /*enum class LightSourceData::TypeRepere{
    MONDE = 0, CAMERA = 1
  };*/
bool LightSourceData::AddSource(int typeRepere, int lightId,
               float lightPositionX, float lightPositionY, float lightPositionZ,
               float diffuseIntensityR, float diffuseIntensityG, float diffuseIntensityB,
               float specularIntensityR, float specularIntensityG, float specularIntensityB){
  // Test d'exitence de la source dans l'autre repère que celui spécifié
  vector<LightSourceData::PointLightSource> sourcesAutre=GetSourcesByRepere(typeRepere, true);
  vector<LightSourceData::PointLightSource>::iterator it;
  
  // Test de doublon pour l'ID de la source
  for (it = sourcesAutre.begin(); it != sourcesAutre.end() ; it++){
    if (it->getLightId() == lightId){
      return false;
    }
  }
  // Test d'exitence de la source dans le même repère que celui spécifié
  vector<LightSourceData::PointLightSource>  sources = GetSourcesByRepere(typeRepere,false);
  //vector<LightSourceData::PointLightSource>::iterator it;
  // Test de doublon pour l'ID de la source
  for ( it = sources.begin() ; it != sources.end() ; ++it){
    if (it->getLightId() == lightId){
      return false;
    }
  }
  // Ajout effectif de la source lumineuse dans la bonne collection
  GetSourcesByRepere(typeRepere).push_back(
          PointLightSource(lightId,
                           lightPositionX, lightPositionY, lightPositionZ,
                           diffuseIntensityR, diffuseIntensityG, diffuseIntensityB,
                          specularIntensityR, specularIntensityG, specularIntensityB));
  return true ;
}

std::vector<LightSourceData::PointLightSource>& LightSourceData
                      ::GetSourcesByRepere(int typeRepere,
                                                  bool reverse) {
  if (typeRepere == 1){
    return reverse ? mSourcesRepereMonde : mSourcesRepereCamera;
  }
  return reverse ? mSourcesRepereCamera : mSourcesRepereMonde;
}

const std::vector<LightSourceData::PointLightSource>& LightSourceData
                      ::GetSourcesByRepere(int typeRepere,
                                                  bool reverse) const {
  if (typeRepere == 1){
    return reverse ? mSourcesRepereMonde : mSourcesRepereCamera;
  }
  return reverse ? mSourcesRepereCamera : mSourcesRepereMonde;
}

bool LightSourceData::DeleteSource(int lightId){
  vector<PointLightSource> sourcesCamera = mSourcesRepereCamera;
  vector<PointLightSource>::iterator it;
  // Test de doublon pour l'ID de la source
  for (it = sourcesCamera.begin() ; it != sourcesCamera.end() ; ++it){
    if (it->getLightId() == lightId){
      sourcesCamera.erase(it); // suppression de l'élément
      return true;
    }
  }
  vector<PointLightSource>& sourcesMonde = mSourcesRepereMonde;
  // Test de doublon pour l'ID de la source
  for (it = sourcesMonde.begin() ; it != sourcesMonde.end() ; ++it){
    if (it->getLightId() == lightId){
      sourcesMonde.erase(it);
      return true;
    }
  }
  return false;
}

void LightSourceData::ApplyLightPositions(int typeRepere) const {
  //auto sources = GetSourcesByRepere(typeRepere);
  vector<LightSourceData::PointLightSource> sources=GetSourcesByRepere(typeRepere);
  vector<LightSourceData::PointLightSource>::iterator it;
    for ( it = sources.begin() ; it != sources.end() ; ++it){
    it->ApplyPosition();
  }
}

void LightSourceData::ApplyLightIntensities() const {
  vector<PointLightSource> sourcesCamera = mSourcesRepereCamera;
  vector<PointLightSource>::iterator it;
  for (it = sourcesCamera.begin() ; it != sourcesCamera.end() ; ++it){
    it->ApplyIntensity();
  }
  vector<PointLightSource> sourcesMonde = mSourcesRepereMonde;
  for (it = sourcesMonde.begin() ; it != sourcesMonde.end() ; ++it){
    it->ApplyIntensity();
  }
}

void LightSourceData::DisableLightSources(
                          int typeRepere){
 
  vector<LightSourceData::PointLightSource> sources=GetSourcesByRepere(typeRepere);
  vector<LightSourceData::PointLightSource>::iterator it;
  for ( it = sources.begin() ; it != sources.end() ; ++it){
    it->Disable();
  }
}
