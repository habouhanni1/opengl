/******************************************************************************\
*     Copyright (C) 2017 by Rémy Malgouyres                                    * 
*     http://malgouyres.org                                                    * 
*     File: Modele.cpp                                                         * 
*                                                                              * 
* The program is distributed under the terms of the GNU General Public License * 
*                                                                              * 
\******************************************************************************/

#include <stdio.h>
#include <math.h>
#include "Modele.h"
#include "FramesData.h"
double zCam=-600.0;

Modele::Modele(u_int32_t largeurFenetre, u_int32_t hauteurFenetre) :
        mLargeurFenetre(largeurFenetre),
        mHauteurFenetre(hauteurFenetre),
        mNiveauGris(0.0f),
        mAngleRotation(0.0),
        mVitesseRotation(0.3),
        mSelection(0),
        mDisplayManager(),
        mCamera((50.0 * M_PI) / 180, ((double) largeurFenetre) / hauteurFenetre, 0.2, 100000.0,
                0.0, 0.0, zCam*9,
                0.0, 0.0, 0.0,
                0.0, 1.0, 0.0),
        mlightSources(),
        mDefaultMaterial(0.2f,0.2f,0.2f,
						1.0f,1.0f,1.0f,
						1.0f,1.0f,1.0f,
						110.0f)// Const,ruction du gestionnaire de vue
{
    FramesData::Init();// Initialisation du compteur de frames
    WrapperAssimp::getInstance()->rechargeScene("./fichiers3DS/greek_sculpture.3ds");
    if(!mlightSources.AddSource(1,
								GL_LIGHT0,
								0.0f,0.0f,10.0f,
								0.6f,0.6f,0.6f,
								0.6f,0.6f,0.6f))
								{
									fprintf(stderr,"impossible b3da mni");
								}
}

float Modele::getNiveauGris() const {
    return mNiveauGris;
}

u_int32_t Modele::getLargeurFenetre() const {
    return mLargeurFenetre;
}

u_int32_t Modele::getHauteurFenetre() const {
    return mHauteurFenetre;
}

void Modele::Update() {
    // Affichage des Frames par seconde (FPS)
    if (FramesData::Update()) {
        fprintf(stderr, "%s\n", FramesData::getDescriptionFPS());
    }
    // Données d'environnement :
    mNiveauGris += 0.005f;
    if (mNiveauGris > 1.0f) {
        mNiveauGris = 0.0f;
    }
    mAngleRotation += mVitesseRotation;
    mDisplayManager.Affichage(*this); // Mise à jour de la vue
}

void Modele::UpdateMouseMotion(int xShift, int yShift) {
    mCamera.SetpointDeViseX(mCamera.getMPointDeVisee()[0] + xShift);
    mCamera.SetpointDeViseZ(mCamera.getMPointDeVisee()[2] + yShift);
}

void Modele::Redimensionnement(u_int32_t largeurFenetre, u_int32_t hauteurFenetre) {
    mLargeurFenetre = largeurFenetre;
    mHauteurFenetre = hauteurFenetre;
    mCamera.SetMAspect(getLargeurFenetre()/(double)getHauteurFenetre());
    mDisplayManager.setWindowChanged();
}

const Camera &Modele::getCamera() const {
    return mCamera;
}

void Modele::ApplyModelTransform() const {
}

const DisplayManager &Modele::getMDisplayManager() const {
    return mDisplayManager;
}

double Modele::getMAngleRotation() const {
    return mAngleRotation;
}

void Modele::rotateView(double change) {
    mCamera.SetpositionX(9*zCam*sin(change));
    mCamera.SetpositionZ(-9*zCam*cos(change));
    mCamera.ApplyCameraCoordinates();
}

int Modele::getSelection() const {
    return mSelection;
}

void Modele::SetSelection(int selection) {
    Modele::mSelection = selection;
}


const LightSourceData &Modele::getLightSources() const{
	return mlightSources;
}
    
const Material &Modele::getDefaultMaterial() const{
	return mDefaultMaterial;
}

