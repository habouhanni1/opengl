

#ifndef HEADER_MODELE_H
#define HEADER_MODELE_H

#include "DisplayManager.h"
#include "Camera.h"

#include "LightSourceData.h"
#include "Material.h"
#include "WrapperAssimp.h"
/**
 * Modèle de données
 */
class Modele {

public:
    /** @brief  Constructeur prenant en paramètre la géométrie de la fenêtre
     * Initialise les données de l'application et données nécessaires à l'affichage.
     * @param largeurFenetre largeur de la fenêtre graphique en pixels
     * @param hauteurFenetre hauteur de la fenêtre graphique en pixels
     **/
    Modele(u_int32_t largeurFenetre, u_int32_t hauteurFenetre);
    // //////// Accesseurs
    float getNiveauGris() const;
    u_int32_t getLargeurFenetre() const;
    u_int32_t getHauteurFenetre() const;
    /** Mise à jour du modèle invoquée à chaque événement timer */
    void Update();
    /** Mise à jour du modèle invoquée à chaque événement souris
     * @param xShift Variation de la coordonnée x de la souris
     * @param yShift Variation de la coordonnée y de la souris
     **/
    void UpdateMouseMotion(int xShift, int yShift);
    /** @brief Réglage du cadrage pour la vue
     * Doit être rappelée si la taille de la vue change (SDL_WINDOWEVENT)
     * @param largeurFenetre largeur de la fenêtre graphique en pixels
     * @param hauteurFenetre hauteur de la fenêtre graphique en pixels
     */
    void Redimensionnement(u_int32_t largeurFenetre, u_int32_t hauteurFenetre);
    
    const Camera &getCamera() const;
    void ApplyModelTransform() const;
    const DisplayManager &getMDisplayManager() const;

    double getMAngleRotation() const;

    void rotateView(double change);

    int getSelection() const;

    void SetSelection(int selection);
    
    const LightSourceData &getLightSources() const;
    
    const Material &getDefaultMaterial() const;

private:
/** largeur de la fenêtre graphique en pixels */
    u_int32_t mLargeurFenetre;
    /** hauteur de la fenêtre graphique en pixels */
    u_int32_t mHauteurFenetre;
    /** Niveau de gris du fond */
    float mNiveauGris;
    /** Gestionnaire de la vue (affichage) */
    double mAngleRotation;
    double mVitesseRotation;
    int mSelection;
    DisplayManager mDisplayManager;
    //reference to Camera
    Camera mCamera;
    LightSourceData mlightSources;
    Material mDefaultMaterial;
};

#endif
